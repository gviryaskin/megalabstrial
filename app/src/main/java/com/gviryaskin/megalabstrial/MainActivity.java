package com.gviryaskin.megalabstrial;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.gviryaskin.megalabstrial.model.Request;
import com.gviryaskin.megalabstrial.model.Response;
import com.gviryaskin.megalabstrial.provider.ApiProvider;
import com.gviryaskin.megalabstrial.provider.StorageProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

public class MainActivity extends AppCompatActivity {
    private final static SimpleDateFormat LOG_TIME_FORMAT = new SimpleDateFormat("hh:mm:ss");
    @Bind(R.id.btn_auth)
    protected Button mAuth;

    @Bind(R.id.btn_request)
    protected Button mRequest;

    @Bind(R.id.lv_log_entry)
    protected ListView mListView;

    @Inject
    protected ApiProvider mApiProvider;

    @Inject
    protected StorageProvider mStorageProvider;
    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle aSavedInstanceState) {
        super.onCreate(aSavedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ButterKnife.bind(this);
        ((App) getApplication()).getObjectGraph().inject(this);

        boolean isUserLogged = mStorageProvider.getUser() != null;
        mAuth.setText(isUserLogged ? R.string.btn_log_out : R.string.btn_log_in);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.item_log_entry, R.id.tv_log_entry);
        mListView.setAdapter(adapter);

        addLogEntry(isUserLogged ? R.string.log_user_logged : R.string.log_user_is_not_logged);

        mCompositeSubscription.add(mApiProvider.getResponseSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response aResponse) {
                        switch (aResponse.getType()) {
                            case LOGIN:
                                synchronized (mStorageProvider) {
                                    addLogEntry(R.string.log_user_logged);
                                    sendCachedRequests();
                                    mStorageProvider.addUser(aResponse);
                                    mAuth.setEnabled(true);
                                    mAuth.setText(R.string.btn_log_out);
                                }
                                break;
                            case LOGOUT:
                                addLogEntry(R.string.log_user_is_not_logged);
                                mStorageProvider.removeUser();
                                mAuth.setEnabled(true);
                                mAuth.setText(R.string.btn_log_in);
                                break;
                            case RESPONSE:
                                addLogEntry(aResponse.toString());
                                break;
                        }
                    }
                }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeSubscription.unsubscribe();
    }

    protected void sendCachedRequests() {
        List<Request> cachedRequest;
        do {
            cachedRequest = mStorageProvider.getCachedRequests();
            for (Request request : cachedRequest) {
                mStorageProvider.removeRequest(request);
                mApiProvider.makeRequest(request);
            }
        } while (!cachedRequest.isEmpty());
    }


    protected void addLogEntry(int aLogResId) {
        addLogEntry(getString(aLogResId));
    }

    protected void addLogEntry(@NonNull String aLogEntry) {
        ((ArrayAdapter<String>) mListView.getAdapter()).add(
                String.format("%s:%s", LOG_TIME_FORMAT.format(new Date()), aLogEntry));
    }

    @OnClick(R.id.btn_auth)
    protected void onAuthClicked() {
        mAuth.setEnabled(false);
        if (mStorageProvider.getUser() == null) {
            addLogEntry(R.string.log_login_request);
            mApiProvider.login();
        } else {
            addLogEntry(R.string.log_logout_request);
            mApiProvider.logout();
        }

    }

    @OnClick(R.id.btn_request)
    protected void onRequestClicked() {
        synchronized (mStorageProvider) {
            Request request = new Request();
            if (mStorageProvider.getUser() == null) {
                addLogEntry(getString(R.string.log_cache_request) + request.getId());
                mStorageProvider.cacheRequest(request);
            } else {
                addLogEntry(getString(R.string.log_make_request) + request.getId());
                mApiProvider.makeRequest(request);
            }
        }
    }

}
