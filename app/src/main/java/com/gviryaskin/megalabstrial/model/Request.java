package com.gviryaskin.megalabstrial.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by gviryaskin on 06.01.16.
 */
@DatabaseTable(tableName = "request")
public class Request {
    @DatabaseField(columnName = "id", unique = true, id = true)
    private int mId;
    @DatabaseField(columnName = "time_stamp")
    private long mTimeStamp;

    public Request() {
        mId = hashCode();
        mTimeStamp = System.currentTimeMillis();
    }

    public int getId() {
        return mId;
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

}
