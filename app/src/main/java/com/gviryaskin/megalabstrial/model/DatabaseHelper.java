package com.gviryaskin.megalabstrial.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by gviryaskin on 08.01.16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public DatabaseHelper(Context aContext) {
        super(aContext,
                "database.db",
                null,
                1);
    }

    @Override
    public void onCreate(SQLiteDatabase aDatabase, ConnectionSource aConnectionSource) {
        try {
            TableUtils.createTable(aConnectionSource, Request.class);
            TableUtils.createTable(aConnectionSource, User.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Unable to create databases", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase aDatabase, ConnectionSource aConnectionSource, int aOldVersion, int aNewVersion) {
        try {
            TableUtils.dropTable(aConnectionSource, Request.class, true);
            TableUtils.dropTable(aConnectionSource, User.class, true);

            TableUtils.createTable(aConnectionSource, Request.class);
            TableUtils.createTable(aConnectionSource, User.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Unable to create databases", e);
        }
    }
}
