package com.gviryaskin.megalabstrial.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by gviryaskin on 06.01.16.
 */
@DatabaseTable(tableName = "user")
public class User {
    public final static int TEST_ID = 0;

    @DatabaseField(columnName = "id", id = true)
    private int mId;

    public User() {
        mId = TEST_ID;
    }

    public int getId() {
        return mId;
    }
}
