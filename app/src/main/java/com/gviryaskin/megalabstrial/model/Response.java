package com.gviryaskin.megalabstrial.model;

import android.support.annotation.NonNull;

/**
 * Created by gviryaskin on 06.01.16.
 */
public class Response {
    private int mId;
    private Type mType;
    private String mResult;

    public Response(int aId, @NonNull String aResult, @NonNull Type aType) {
        mId = aId;
        mResult = aResult;
        mType = aType;
    }

    @NonNull
    public int getId() {
        return mId;
    }

    @NonNull
    public String getResult() {
        return mResult;
    }

    @NonNull
    public Type getType() {
        return mType;
    }

    @Override
    public String toString() {
        return String.format("Response:%d with result:%s", mId, mResult);
    }

    public enum Type {LOGIN, LOGOUT, RESPONSE}
}
