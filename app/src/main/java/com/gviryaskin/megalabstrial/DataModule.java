package com.gviryaskin.megalabstrial;

import com.gviryaskin.megalabstrial.provider.ApiProvider;
import com.gviryaskin.megalabstrial.provider.StorageProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by gviryaskin on 06.01.16.
 */
@Module(injects = MainActivity.class)
public class DataModule {

    protected App mApp;

    public DataModule(App aApp) {
        mApp = aApp;
    }

    @Provides
    @Singleton
    public ApiProvider provideApi() {
        return new ApiProvider(mApp);
    }

    @Provides
    @Singleton
    public StorageProvider provideStorage() {
        return new StorageProvider(mApp);
    }
}
