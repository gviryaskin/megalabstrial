package com.gviryaskin.megalabstrial.provider;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.gviryaskin.megalabstrial.App;
import com.gviryaskin.megalabstrial.model.DatabaseHelper;
import com.gviryaskin.megalabstrial.model.Request;
import com.gviryaskin.megalabstrial.model.Response;
import com.gviryaskin.megalabstrial.model.User;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gviryaskin on 06.01.16.
 */
public class StorageProvider {
    protected App mApp;
    private DatabaseHelper mDatabaseHelper;

    public StorageProvider(App aApp) {
        mApp = aApp;
        mDatabaseHelper = new DatabaseHelper(aApp);
    }

    protected Dao<User, Integer> getUserDao() throws SQLException {
        return mDatabaseHelper.getDao(User.class);
    }

    protected Dao<Request, Integer> getRequestDao() throws SQLException {
        return mDatabaseHelper.getDao(Request.class);
    }

    @Nullable
    public User getUser() {
        try {
            return getUserDao().queryForId(User.TEST_ID);
        } catch (SQLException e) {
            Log.e("DAO", e.getMessage());
        }
        return null;
    }

    public void cacheRequest(Request aRequest) {
        try {
            getRequestDao().create(aRequest);
        } catch (SQLException e) {
            Log.e("DAO", e.getMessage());
        }
    }

    public void removeUser() {
        try {
            getUserDao().deleteById(User.TEST_ID);
        } catch (SQLException e) {
            Log.e("DAO", e.getMessage());
        }
    }

    public void addUser(Response aResponse) {
        User user = new User();
        try {
            getUserDao().create(user);
        } catch (SQLException e) {
            Log.e("DAO", e.getMessage());
        }

    }

    @NonNull
    public List<Request> getCachedRequests() {
        try {
            return getRequestDao().queryForAll();
        } catch (SQLException e) {
            Log.e("DAO", e.getMessage());
            return new ArrayList<>();
        }
    }

    public void removeRequest(Request aRequest) {
        try {
            getRequestDao().delete(aRequest);
        } catch (SQLException e) {
            Log.e("DAO", e.getMessage());
        }
    }

}
