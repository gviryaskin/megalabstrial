package com.gviryaskin.megalabstrial.provider;

import android.support.annotation.NonNull;

import com.gviryaskin.megalabstrial.App;
import com.gviryaskin.megalabstrial.model.Request;
import com.gviryaskin.megalabstrial.model.Response;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by gviryaskin on 06.01.16.
 */
public class ApiProvider {
    private final static String RESPONSE_PLUG = "success";
    protected App mApp;
    private BehaviorSubject<Response> mResponseSubject = BehaviorSubject.create();
    private ThreadPoolExecutor mThreadPoolExecutor = new ThreadPoolExecutor(1, 1, 3, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    public ApiProvider(App aApp) {
        mApp = aApp;
    }

    public BehaviorSubject<Response> getResponseSubject() {
        return mResponseSubject;
    }

    //TODO add debonse for login and logout
    private void makeFakeRequest(@NonNull Action1<Request> aAction, @NonNull Request aRequest) {
        Observable.just(aRequest)
                .subscribeOn(Schedulers.from(mThreadPoolExecutor))
                .delaySubscription(2, TimeUnit.SECONDS)
                .subscribe(aAction);
    }

    public void login() {
        makeFakeRequest(new Action1<Request>() {
            @Override
            public void call(Request aRequest) {
                Response response = new Response(aRequest.getId(), RESPONSE_PLUG, Response.Type.LOGIN);
                mResponseSubject.onNext(response);
            }
        }, new Request());
    }

    public void logout() {
        makeFakeRequest(new Action1<Request>() {
            @Override
            public void call(Request aRequest) {
                Response response = new Response(aRequest.getId(), RESPONSE_PLUG, Response.Type.LOGOUT);
                mResponseSubject.onNext(response);
            }
        }, new Request());
    }

    public void makeRequest(@NonNull Request aRequest) {
        makeFakeRequest(new Action1<Request>() {
            @Override
            public void call(Request aRequest) {
                Response response = new Response(aRequest.getId(), RESPONSE_PLUG, Response.Type.RESPONSE);
                mResponseSubject.onNext(response);
            }
        }, aRequest);
    }
}
