package com.gviryaskin.megalabstrial;

import android.app.Application;

import dagger.ObjectGraph;

/**
 * Created by gviryaskin on 06.01.16.
 */
public class App extends Application {

    private ObjectGraph mObjectGraph;

    public ObjectGraph getObjectGraph() {
        if (mObjectGraph == null) {
            mObjectGraph = ObjectGraph.create(new DataModule(this));
        }
        return mObjectGraph;
    }

}
